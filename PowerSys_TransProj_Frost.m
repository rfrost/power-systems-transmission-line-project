% Ryan Frost
% EE30372
% Project 1, Trasmission Line
% Due: 4/19/2021

clear;
clc;

%% define given parameters/constants
% given distances for trans. lines
milesA = 89;    % A: Line from Cook Plant to Battle Creek
milesB = 122;   % B: Line from Battle Creek to Detroit

% distances for trans. lines conv. to km
kmA = milesA * 1.60934;
kmB = milesB * 1.60934;

% AC transmission voltages (kV)
V1 = 138 * 10^3;
V2 = 345 * 10^3;
V3 = 765 * 10^3;
V_DC = 700 * 10^3;

% power delivered (W)
P_Det = 300 * 10^6;
P_BC = 300 * 10^6;

% power factor is defined as PF 0.9 lagging for AC trans., 1.0 for DC
PF_AC = 0.9;
PF_DC = 1.0;

% theta corresp. to PF 0.9
theta = acosd(PF_AC);

% U.S. AC power generation/transmission frequency (Hz)
f = 60;

%% simulation/calculations
% compute:
    % currents/losses in each line for all four options
    % voltage at each bus for all four options
    % voltage regulation for all four options
    % for AC cases, charging currents for each line
% check: given that voltage @ Cook Plantis 419.73<20.78 kV for 345 kV case

% find apparent power magnitude at each facility
% both Battle Creek and Detroit receiving 300 MW at PF 0.9
S_Det = P_Det/PF_AC;
S_BC_DC = P_BC/PF_AC;


%% 138 kV case
% from Table 3-1
r1 = 0.1688;    % line resistance, ohms per mile
l1 = 0.789;     % line inductance, ohms per mile
c1 = 0.186;     % line capacitance, M-ohms-mile to neutral

% 138 kV, Bridgman to Battle Creek line (A)
R_1A = r1 * milesA;
L_1A = l1 * milesA;
Cs_1A = (c1 / milesA) * 10^6;
Z_1A = R_1A + 1j*L_1A;
Y_1A = 1j*(Cs_1A)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_1A = (Z_1A * Y_1A)/2 + 1;
B_1A = Z_1A;
C_1A = (Y_1A)*(((Z_1A * Y_1A)/(4)) + 1);
D_1A = A_1A;
ABCD_1A = [A_1A B_1A;
           C_1A D_1A];

% 138 kV, Battle Creek to Detroit line (B)
R_1B = r1 * milesB;
L_1B = l1 * milesB;
Cs_1B = (c1 / milesB) * 10^6;
Z_1B = R_1B + 1j*L_1B;
Y_1B = 1j*(Cs_1B)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_1B = (Z_1B * Y_1B)/2 + 1;
B_1B = Z_1B;
C_1B = (Y_1B)*(((Z_1B * Y_1B)/(4)) + 1);
D_1B = A_1B;
ABCD_1B = [A_1B B_1B;
           C_1B D_1B];
       
% receiving current at Detroit
Il_Det1 = (S_Det)/(sqrt(3)*V1);
Ir_Det1 = Il_Det1*cosd(-1*theta) + 1j*Il_Det1*sind(-1*theta); % negative angle, lagging

% receiving voltage at Detroit
Vr_Det1 = V1/sqrt(3);
r_Det1 = [Vr_Det1; Ir_Det1];
% convert to polar, scale up voltage to line-to-line
mag_Vr_Det1 = sqrt(3)*Vr_Det1;
angle_Vr_Det1 = 0;
[mag_Ir_Det1, angle_Ir_Det1] = r2p2(r_Det1(2));

% working "upstream" sending voltage from Battle Creek
s_BC1 = ABCD_1B * r_Det1;
Vs_BC1 = s_BC1(1);
Is_BC1 = s_BC1(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_BC1, angle_Vs_BC1] = r2p2(sqrt(3)*Vs_BC1);
[mag_Is_BC1, angle_Is_BC1] = r2p2(Is_BC1);

% Battle Creek unit, 300 MW at PF 0.9 lagging
% calculate line current into Battle Creek generator
Il_BC1 = (S_BC_DC)/(sqrt(3)*mag_Vs_BC1);
% current phase at Battle Creek
theta1 = angle_Vs_BC1 - theta;
% current into Battle Creek
I_BC1 = Il_BC1*cosd(theta1) + 1j*Il_BC1*sind(theta1);
% voltage at Battle Creek load same as sending voltage out
V_BC1 = Vs_BC1;
% convert to polar, scale up voltage to line-to-line
[mag_V_BC1, angle_V_BC1] = r2p2(sqrt(3)*V_BC1);
[mag_I_BC1, angle_I_BC1] = r2p2(I_BC1);

% KCL at Battle Creek node to find receiving current at Battle Creek
Ir_BC1 = Is_BC1 + I_BC1;

% receiving voltage at Battle Creek
Vr_BC1 = Vs_BC1;
r_BC1 = [Vr_BC1; Ir_BC1];
% convert to polar, scale up voltage to line-to-line
[mag_Vr_BC1, angle_Vr_BC1] = r2p2(sqrt(3)*r_BC1(1));
[mag_Ir_BC1, angle_Ir_BC1] = r2p2(r_BC1(2));

% again, working "upstream" to find sending voltage/current from Cook Plant
s_Cook1 = ABCD_1A * r_BC1;
Vs_Cook1 = s_Cook1(1);
Is_Cook1 = s_Cook1(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_Cook1, angle_Vs_Cook1] = r2p2(sqrt(3)*Vs_Cook1);
[mag_Is_Cook1, angle_Is_Cook1] = r2p2(Is_Cook1);

% charging currents, 138 kV case
% Ic = V * (Y/2)
% through shunt admittance at Cook Plant side of line A (amps)
Ic_Cook_1A = Vs_Cook1 * (Y_1A/2);
[mag_Ic_Cook_1A, angle_Ic_Cook_1A] = r2p2(Ic_Cook_1A);
% through shunt admittance at Battle Creek side of line A (amps)
Ic_BC_1A = Vr_BC1 * (Y_1A/2);
[mag_Ic_BC_1A, angle_Ic_BC_1A] = r2p2(Ic_BC_1A);
% through shunt admittance at Battle Creek side of line B (amps)
Ic_BC_1B = Vs_BC1 * (Y_1B/2);
[mag_Ic_BC_1B, angle_Ic_BC_1B] = r2p2(Ic_BC_1B);
% through shunt admittance at Detroit side of line B (amps)
Ic_Det_1B = Vr_Det1 * (Y_1B/2);
[mag_Ic_Det_1B, angle_Ic_Det_1B] = r2p2(Ic_Det_1B);

% line losses, 138 kV case
% P_loss = I^2 * R
% line A losses (W)
I_line1A = Is_Cook1 - Ic_Cook_1A; % from KCL
[mag_I_line1A, angle_I_line1A] = r2p2(I_line1A);
P_loss_1A = (mag_I_line1A)^2 * 3*R_1A; % (I^2)(R) losses 
% line B losses (W)
I_line1B = Is_BC1 - Ic_BC_1B; % from KCL
[mag_I_line1B, angle_I_line1B] = r2p2(I_line1B);
P_loss_1B = (mag_I_line1B)^2 * 3*R_1B; % (I^2)(R) losses

% voltage regulation, 138 kV case
% VR = (Vs - Vr)/(Vr) x 100%
VR1 = ((mag_Vs_Cook1 - mag_Vr_Det1)/(mag_Vr_Det1)) * 100;


%% 345 kV case
% from Table 3-1
r2 = 0.0564;
l2 = 0.596;
c2 = 0.142;

% 345 kV, Bridgman to Battle Creek line (A)
R_2A = r2 * milesA;
L_2A = l2 * milesA;
Cs_2A = (c2 / milesA) * 10^6;
Z_2A = R_2A + 1j*L_2A;
Y_2A = 1j*(Cs_2A)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_2A = (Z_2A * Y_2A)/2 + 1;
B_2A = Z_2A;
C_2A = (Y_2A)*(((Z_2A * Y_2A)/(4)) + 1);
D_2A = A_2A;
ABCD_2A = [A_2A B_2A;
           C_2A D_2A];

% 345 kV, Battle Creeek to Detroit line (B)
R_2B = r2 * milesB;
L_2B = l2 * milesB;
Cs_2B = (c2 / milesB) * 10^6;
Z_2B = R_2B + 1j*L_2B;
Y_2B = 1j*(Cs_2B)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_2B = (Z_2B * Y_2B)/2 + 1;
B_2B = Z_2B;
C_2B = (Y_2B)*(((Z_2B * Y_2B)/(4)) + 1);
D_2B = A_2B;
ABCD_2B = [A_2B B_2B;
           C_2B D_2B];
       
% receiving current at Detroit
Il_Det2 = (S_Det)/(sqrt(3)*V2);
Ir_Det2 = Il_Det2*cosd(-1*theta) + 1j*Il_Det2*sind(-1*theta); % negative angle, lagging

% receiving voltage at Detroit
Vr_Det2 = V2/sqrt(3);
r_Det2 = [Vr_Det2; Ir_Det2];
% convert to polar, scale up voltage to line-to-line
mag_Vr_Det2 = sqrt(3)*Vr_Det2;
angle_Vr_Det2 = 0;
[mag_Ir_Det2, angle_Ir_Det2] = r2p2(r_Det2(2));

% working "upstream" sending voltage from Battle Creek
s_BC2 = ABCD_2B * r_Det2;
Vs_BC2 = s_BC2(1);
Is_BC2 = s_BC2(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_BC2, angle_Vs_BC2] = r2p2(sqrt(3)*Vs_BC2);
[mag_Is_BC2, angle_Is_BC2] = r2p2(Is_BC2);

% Battle Creek unit, 300 MW at PF 0.9 lagging
% calculate line current into Battle Creek generator
Il_BC2 = (S_BC_DC)/(sqrt(3)*mag_Vs_BC2);
% current phase at Battle Creek
theta2 = angle_Vs_BC2 - theta;
% current into Battle Creek
I_BC2 = Il_BC2*cosd(theta2) + 1j*Il_BC2*sind(theta2);
% voltage at Battle Creek load same as sending voltage out
V_BC2 = Vs_BC2;
% convert to polar, scale up voltage to line-to-line
[mag_V_BC2, angle_V_BC2] = r2p2(sqrt(3)*V_BC2);
[mag_I_BC2, angle_I_BC2] = r2p2(I_BC2);

% KCL at Battle Creek node to find receiving current at Battle Creek
Ir_BC2 = Is_BC2 + I_BC2;

% receiving voltage at Battle Creek
Vr_BC2 = Vs_BC2;
r_BC2 = [Vr_BC2; Ir_BC2];
% convert to polar, scale up voltage to line-to-line
[mag_Vr_BC2, angle_Vr_BC2] = r2p2(sqrt(3)*r_BC2(1));
[mag_Ir_BC2, angle_Ir_BC2] = r2p2(r_BC2(2));

% again, working "upstream" to find sending voltage/current from Cook Plant
s_Cook2 = ABCD_2A * r_BC2;
Vs_Cook2 = s_Cook2(1);
Is_Cook2 = s_Cook2(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_Cook2, angle_Vs_Cook2] = r2p2(sqrt(3)*Vs_Cook2);
[mag_Is_Cook2, angle_Is_Cook2] = r2p2(Is_Cook2);

% charging currents, 345 kV case
% Ic = V * (Y/2)
% through shunt admittance at Cook Plant side of line A (amps)
Ic_Cook_2A = Vs_Cook2 * (Y_2A/2);
[mag_Ic_Cook_2A, angle_Ic_Cook_2A] = r2p2(Ic_Cook_2A);
% through shunt admittance at Battle Creek side of line A (amps)
Ic_BC_2A = Vr_BC2 * (Y_2A/2);
[mag_Ic_BC_2A, angle_Ic_BC_2A] = r2p2(Ic_BC_2A);
% through shunt admittance at Battle Creek side of line B (amps)
Ic_BC_2B = Vs_BC2 * (Y_2B/2);
[mag_Ic_BC_2B, angle_Ic_BC_2B] = r2p2(Ic_BC_2B);
% through shunt admittance at Detroit side of line B (amps)
Ic_Det_2B = Vr_Det2 * (Y_2B/2);
[mag_Ic_Det_2B, angle_Ic_Det_2B] = r2p2(Ic_Det_2B);

% line losses, 345 kV case
% P_loss = I^2 * R
% line A losses (W)
I_line2A = Is_Cook2 - Ic_Cook_2A; % from KCL
[mag_I_line2A, angle_I_line2A] = r2p2(I_line2A);
P_loss_2A = (mag_I_line2A)^2 * 3*R_2A; % (I^2)(R) losses 
% line B losses (W)
I_line2B = Is_BC2 - Ic_BC_2B; % from KCL
[mag_I_line2B, angle_I_line2B] = r2p2(I_line2B);
P_loss_2B = (mag_I_line2B)^2 * 3*R_2B; % (I^2)(R) losses

% voltage regulation, 345 kV case
% VR = (Vs - Vr)/(Vr) x 100%
VR2 = ((mag_Vs_Cook2 - mag_Vr_Det2)/(mag_Vr_Det2)) * 100;


%% 765 kV case
% from Table 3-1
r3 = 0.0201;
l3 = 0.535;
c3 = 0.129;

% 765 kV, Bridgman to Battle Creek line (A)
R_3A = r3 * milesA;
L_3A = l3 * milesA;
Cs_3A = (c3 / milesA) * 10^6;
Z_3A = R_3A + 1j*L_3A;
Y_3A = 1j*(Cs_3A)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_3A = (Z_3A * Y_3A)/2 + 1;
B_3A = Z_3A;
C_3A = (Y_3A)*(((Z_3A * Y_3A)/(4)) + 1);
D_3A = A_3A;
ABCD_3A = [A_3A B_3A;
           C_3A D_3A];

% 765 kV, Battle Creek to Detroit line (B)
R_3B = r3 * milesB;
L_3B = l3 * milesB;
Cs_3B = (c3 / milesB) * 10^6;
Z_3B = R_3B + 1j*L_3B;
Y_3B = 1j*(Cs_3B)^-1;
% ABCD constants, modeling as a medium-length line (80 km < dist < 240 km)
A_3B = (Z_3B * Y_3B)/2 + 1;
B_3B = Z_3B;
C_3B = (Y_3B)*(((Z_3B * Y_3B)/(4)) + 1);
D_3B = A_3B;
ABCD_3B = [A_3B B_3B;
           C_3B D_3B];
 
% receiving current at Detroit
Il_Det3 = (S_Det)/(sqrt(3)*V3);
Ir_Det3 = Il_Det3*cosd(-1*theta) + 1j*Il_Det3*sind(-1*theta); % negative angle, lagging

% receiving voltage at Detroit
Vr_Det3 = V3/sqrt(3);
r_Det3 = [Vr_Det3; Ir_Det3];
% convert to polar, scale up voltage to line-to-line
mag_Vr_Det3 = sqrt(3)*Vr_Det3;
angle_Vr_Det3 = 0;
[mag_Ir_Det3, angle_Ir_Det3] = r2p2(r_Det3(2));

% working "upstream" sending voltage from Battle Creek
s_BC3 = ABCD_3B * r_Det3;
Vs_BC3 = s_BC3(1);
Is_BC3 = s_BC3(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_BC3, angle_Vs_BC3] = r2p2(sqrt(3)*Vs_BC3);
[mag_Is_BC3, angle_Is_BC3] = r2p2(Is_BC3);

% Battle Creek unit, 300 MW at PF 0.9 lagging
% calculate line current into Battle Creek generator
Il_BC3 = (S_BC_DC)/(sqrt(3)*mag_Vs_BC3);
% current phase at Battle Creek
theta3 = angle_Vs_BC3 - theta;
% current into Battle Creek
I_BC3 = Il_BC3*cosd(theta3) + 1j*Il_BC3*sind(theta3);
% voltage at Battle Creek load same as sending voltage out
V_BC3 = Vs_BC3;
% convert to polar, scale up voltage to line-to-line
[mag_V_BC3, angle_V_BC3] = r2p2(sqrt(3)*V_BC3);
[mag_I_BC3, angle_I_BC3] = r2p2(I_BC3);

% KCL at Battle Creek node to find receiving current at Battle Creek
Ir_BC3 = Is_BC3 + I_BC3;

% receiving voltage at Battle Creek
Vr_BC3 = Vs_BC3;
r_BC3 = [Vr_BC3; Ir_BC3];
% convert to polar, scale up voltage to line-to-line
[mag_Vr_BC3, angle_Vr_BC3] = r2p2(sqrt(3)*r_BC3(1));
[mag_Ir_BC3, angle_Ir_BC3] = r2p2(r_BC3(2));

% again, working "upstream" to find sending voltage/current from Cook Plant
s_Cook3 = ABCD_3A * r_BC3;
Vs_Cook3 = s_Cook3(1);
Is_Cook3 = s_Cook3(2);
% convert to polar, scale up voltage to line-to-line
[mag_Vs_Cook3, angle_Vs_Cook3] = r2p2(sqrt(3)*Vs_Cook3);
[mag_Is_Cook3, angle_Is_Cook3] = r2p2(Is_Cook3);

% charging currents, 765 kV case
% Ic = V * (Y/2)
% through shunt admittance at Cook Plant side of line A (amps)
Ic_Cook_3A = Vs_Cook3 * (Y_3A/2);
[mag_Ic_Cook_3A, angle_Ic_Cook_3A] = r2p2(Ic_Cook_3A);
% through shunt admittance at Battle Creek side of line A (amps)
Ic_BC_3A = Vr_BC3 * (Y_3A/2);
[mag_Ic_BC_3A, angle_Ic_BC_3A] = r2p2(Ic_BC_3A);
% through shunt admittance at Battle Creek side of line B (amps)
Ic_BC_3B = Vs_BC3 * (Y_3B/2);
[mag_Ic_BC_3B, angle_Ic_BC_3B] = r2p2(Ic_BC_3B);
% through shunt admittance at Detroit side of line B (amps)
Ic_Det_3B = Vr_Det3 * (Y_3B/2);
[mag_Ic_Det_3B, angle_Ic_Det_3B] = r2p2(Ic_Det_3B);

% line losses, 765 kV case
% P_loss = I^2 * R
% line A losses (W)
I_line3A = Is_Cook3 - Ic_Cook_3A; % from KCL
[mag_I_line3A, angle_I_line3A] = r2p2(I_line3A);
P_loss_3A = (mag_I_line3A)^2 * 3*R_3A; % (I^2)(R) losses 
% line B losses (W)
I_line3B = Is_BC3 - Ic_BC_3B; % from KCL
[mag_I_line3B, angle_I_line3B] = r2p2(I_line3B);
P_loss_3B = (mag_I_line3B)^2 * 3*R_3B; % (I^2)(R) losses

% voltage regulation, 765 kV case
% VR = (Vs - Vr)/(Vr) x 100%
VR3 = ((mag_Vs_Cook3 - mag_Vr_Det3)/(mag_Vr_Det3)) * 100;


%% DC transmission case, 700 kV DC
% DC transmission line has same construction has 765 kv line, but different
% value for DC resistance per mile
r_DC = 0.0190; % ohms per mile

% DC: f = 0, admittance shunts and line inductance not accounted for
% Single-phase: need to consider line coming there and back
R_DC_A = r_DC * 2*milesA;
R_DC_B = r_DC * 2*milesB;

% receiving voltage, current, and power at Detroit
Vr_Det_DC = V_DC;
S_Det_DC = P_Det / PF_DC;
Ir_Det_DC = (S_Det_DC)/(Vr_Det_DC);

% sending voltage, current from Battle Creel
Vs_BC_DC = Vr_Det_DC + R_DC_B * Ir_Det_DC;
Is_BC_DC = Ir_Det_DC;

% voltage, current at Battle Creek unit
V_BC_DC = Vs_BC_DC;
S_BC_DC = P_BC / PF_DC;
I_BC_DC = (S_BC_DC)/(V_BC_DC);

% KCL at Battle Creek
Ir_BC_DC = I_BC_DC + Is_BC_DC;

% receiving voltage at Battle Creek
Vr_DC = V_BC_DC;

% sending voltage from Cook Plant
Vs_Cook_DC = Vr_DC + R_DC_A * Ir_BC_DC;
Is_Cook_DC = Ir_BC_DC;

% voltage regulation, 700 kV DC case
% VR = (Vs - Vr)/(Vr) x 100%
VR_DC = (Vs_Cook_DC - Vr_Det_DC)/(Vr_Det_DC) * 100;

% line currents
I_line_A_DC = Ir_BC_DC;
I_line_B_DC = Ir_Det_DC;

% line losses (2 lines)
% from Cook Plant to Battle Creek (line A)
P_loss_A_DC = (I_line_A_DC)^2 * R_DC_A;
% from Battle Creek to Detroit (line B)
P_loss_B_DC = (I_line_B_DC)^2 * R_DC_B;


%% DISPLAY RESULTS
disp('Ryan Frost');
disp('EE 30372');
disp('Project 1 - Transmission Line');
disp(['4/19/2021', + newline]);

% 138 kV case
disp(['-------------------- RESULTS - 138 kV case: --------------------', + newline]);
disp('Sending and receiving currents:');
disp(['Sending current from Cook Plant:                      Is_Cook1 = ', num2str(mag_Is_Cook1), '<', num2str(angle_Is_Cook1), ' A']);
disp(['Receiving current at Battle Creek:                    Ir_BC1 = ', num2str(mag_Ir_BC1), '<', num2str(angle_Ir_BC1), ' A']);
disp(['Current into Battle Creek unit:                       I_BC1 = ', num2str(mag_I_BC1), '<', num2str(angle_I_BC1), ' A']);
disp(['Sending current from Battle Creek:                    Is_BC1 = ', num2str(mag_Is_BC1), '<', num2str(angle_Is_BC1), ' A']);
disp(['Receiving current at Detroit:                         Ir_Det1 = ', num2str(mag_Ir_Det1), '<', num2str(angle_Ir_Det1), ' A', + newline]);
disp('Currents in each line:');
disp(['Line current from Cook to Battle Creek (line A):      I_line1A = ', num2str(mag_I_line1A), '<', num2str(angle_I_line1A), ' A']);
disp(['Line current from Battle Creek to Detroit (line B):   I_line1B = ', num2str(mag_I_line1B), '<', num2str(angle_I_line1B), ' A', + newline]);
disp('Bus voltages:');
disp(['Voltage at Cook Plant bus:                            Vs_Cook1 = ', num2str(mag_Vs_Cook1), '<', num2str(angle_Vs_Cook1), ' V']);
disp(['Voltage at Battle Creek bus:                          V_BC1 = Vs_BC1 = Vr_BC1 = ', num2str(mag_V_BC1), '<', num2str(angle_V_BC1), ' V']);
disp(['Voltage at Detroit bus:                               Vr_Det1 = ', num2str(mag_Vr_Det1), '<', num2str(angle_Vr_Det1), ' V', + newline]);
disp('Voltage regulation (VR):');
disp(['Voltage regulation:                                   VR = ', num2str(VR1), '%', + newline]);
disp('Line losses:');
disp(['Lines loss b/t Cook Plant and Battle Creek (line A):  P_loss_1A = ', num2str(P_loss_1A), ' W']);
disp(['Lines loss b/t Battle Creek and Detroit (line B):     P_loss_1B = ', num2str(P_loss_1B), ' W', + newline]);
disp('Charging currents:');
disp(['Current through shunt at Cook Plant side of line A:   Ic_Cook_1A = ', num2str(mag_Ic_Cook_1A), '<', num2str(angle_Ic_Cook_1A), ' A']);
disp(['Current through shunt at Battle Creek side of line A: Ic_BC_1A = ', num2str(mag_Ic_BC_1A), '<', num2str(angle_Ic_BC_1A), ' A']);
disp(['Current through shunt at Cook Plant side of line B:   Ic_BC_1B = ', num2str(mag_Ic_BC_1B), '<', num2str(angle_Ic_BC_1B), ' A']);
disp(['Current through shunt at Detroit side of line B:      Ic_Det_1B = ', num2str(mag_Ic_Det_1B), '<', num2str(angle_Ic_Det_1B), ' A']);
disp(['', + newline, + newline]);

% 345 kV case
disp(['-------------------- RESULTS - 345 kV case: --------------------', + newline]);
disp('Sending and receiving currents:');
disp(['Sending current from Cook Plant:                      Is_Cook2 = ', num2str(mag_Is_Cook2), '<', num2str(angle_Is_Cook2), ' A']);
disp(['Receiving current at Battle Creek:                    Ir_BC2 = ', num2str(mag_Ir_BC2), '<', num2str(angle_Ir_BC2), ' A']);
disp(['Current into Battle Creek unit:                       I_BC2 = ', num2str(mag_I_BC2), '<', num2str(angle_I_BC2), ' A']);
disp(['Sending current from Battle Creek:                    Is_BC2 = ', num2str(mag_Is_BC2), '<', num2str(angle_Is_BC2), ' A']);
disp(['Receiving current at Detroit:                         Ir_Det2 = ', num2str(mag_Ir_Det2), '<', num2str(angle_Ir_Det2), ' A', + newline]);
disp('Currents in each line:');
disp(['Line current from Cook to Battle Creek (line A):      I_line2A = ', num2str(mag_I_line2A), '<', num2str(angle_I_line2A), ' A']);
disp(['Line current from Battle Creek to Detroit (line B):   I_line2B = ', num2str(mag_I_line2B), '<', num2str(angle_I_line2B), ' A', + newline]);
disp('Bus voltages:');
disp(['Voltage at Cook Plant bus:                            Vs_Cook2 = ', num2str(mag_Vs_Cook2), '<', num2str(angle_Vs_Cook2), ' V']);
disp(['Voltage at Battle Creek bus:                          V_BC2 = Vs_BC2 = Vr_BC2 = ', num2str(mag_V_BC2), '<', num2str(angle_V_BC2), ' V']);
disp(['Voltage at Detroit bus:                               Vr_Det2 = ', num2str(mag_Vr_Det2), '<', num2str(angle_Vr_Det2), ' V', + newline]);
disp(['Voltage regulation (VR):                              VR = ', num2str(VR2), '%', + newline]);
disp('Line losses:');
disp(['Lines loss b/t Cook Plant and Battle Creek (line A):  P_loss_2A = ', num2str(P_loss_2A), ' W']);
disp(['Lines loss b/t Battle Creek and Detroit (line B):     P_loss_2B = ', num2str(P_loss_2B), ' W', + newline]);
disp('Charging currents:');
disp(['Current through shunt at Cook Plant side of line A:   Ic_Cook_2A = ', num2str(mag_Ic_Cook_2A), '<', num2str(angle_Ic_Cook_2A), ' A']);
disp(['Current through shunt at Battle Creek side of line A: Ic_BC_2A = ', num2str(mag_Ic_BC_2A), '<', num2str(angle_Ic_BC_2A), ' A']);
disp(['Current through shunt at Cook Plant side of line B:   Ic_BC_2B = ', num2str(mag_Ic_BC_2B), '<', num2str(angle_Ic_BC_2B), ' A']);
disp(['Current through shunt at Detroit side of line B:      Ic_Det_2B = ', num2str(mag_Ic_Det_2B), '<', num2str(angle_Ic_Det_2B), ' A']);
disp(['', + newline, + newline]);

% 765 kV case
disp(['-------------------- RESULTS - 765 kV case: --------------------', + newline]);
disp('Sending and receiving currents:');
disp(['Sending current from Cook Plant:                      Is_Cook3 = ', num2str(mag_Is_Cook3), '<', num2str(angle_Is_Cook3), ' A']);
disp(['Receiving current at Battle Creek:                    Ir_BC3 = ', num2str(mag_Ir_BC3), '<', num2str(angle_Ir_BC3), ' A']);
disp(['Current into Battle Creek unit:                       I_BC3 = ', num2str(mag_I_BC3), '<', num2str(angle_I_BC3), ' A']);
disp(['Sending current from Battle Creek:                    Is_BC3 = ', num2str(mag_Is_BC3), '<', num2str(angle_Is_BC3), ' A']);
disp(['Receiving current at Detroit:                         Ir_Det3 = ', num2str(mag_Ir_Det3), '<', num2str(angle_Ir_Det3), ' A', + newline]);
disp('Currents in each line:');
disp(['Line current from Cook to Battle Creek (line A):      I_line3A = ', num2str(mag_I_line3A), '<', num2str(angle_I_line3A), ' A']);
disp(['Line current from Battle Creek to Detroit (line B):   I_line3B = ', num2str(mag_I_line3B), '<', num2str(angle_I_line3B), ' A', + newline]);
disp('Bus voltages:');
disp(['Voltage at Cook Plant bus:                            Vs_Cook3 = ', num2str(mag_Vs_Cook3), '<', num2str(angle_Vs_Cook3), ' V']);
disp(['Voltage at Battle Creek bus:                          V_BC3 = Vs_BC3 = Vr_BC3 = ', num2str(mag_V_BC3), '<', num2str(angle_V_BC3), ' V']);
disp(['Voltage at Detroit bus:                               Vr_Det3 = ', num2str(mag_Vr_Det3), '<', num2str(angle_Vr_Det3), ' V', + newline]);
disp(['Voltage regulation (VR):                              VR = ', num2str(VR3), '%', + newline]);
disp('Line losses:');
disp(['Lines loss b/t Cook Plant and Battle Creek (line A):  P_loss_3A = ', num2str(P_loss_3A), ' W']);
disp(['Lines loss b/t Battle Creek and Detroit (line B):     P_loss_3B = ', num2str(P_loss_3B), ' W', + newline]);
disp('Charging currents:');
disp(['Current through shunt at Cook Plant side of line A:   Ic_Cook_3A = ', num2str(mag_Ic_Cook_3A), '<', num2str(angle_Ic_Cook_3A), ' A']);
disp(['Current through shunt at Battle Creek side of line A: Ic_BC_3A = ', num2str(mag_Ic_BC_3A), '<', num2str(angle_Ic_BC_3A), ' A']);
disp(['Current through shunt at Cook Plant side of line B:   Ic_BC_3B = ', num2str(mag_Ic_BC_3B), '<', num2str(angle_Ic_BC_3B), ' A']);
disp(['Current through shunt at Detroit side of line B:      Ic_Det_3B = ', num2str(mag_Ic_Det_3B), '<', num2str(angle_Ic_Det_3B), ' A']);
disp(['', + newline,  + newline]);

% 700 kV DC case
disp(['------------------- RESULTS - 700 kV DC case: -------------------', + newline]);
disp('Sending and receiving currents:');
disp(['Sending current from Cook Plant:                      Is_Cook_DC = ', num2str(Is_Cook_DC), ' A']);
disp(['Receiving current at Battle Creek:                    Ir_BC_DC = ', num2str(Ir_BC_DC), ' A']);
disp(['Current into Battle Creek unit:                       I_BC_DC = ', num2str(I_BC_DC), ' A']);
disp(['Sending current from Battle Creek:                    Is_BC_DC = ', num2str(Is_BC_DC), ' A']);
disp(['Receiving current at Detroit:                         Ir_Det_DC = ', num2str(Ir_Det_DC), ' A', + newline]);
disp('Currents in each line:');
disp(['Line current from Cook to Battle Creek (line A):      I_line_A_DC = ', num2str(I_line_A_DC), ' A']);
disp(['Line current from Battle Creek to Detroit (line B):   I_line_A_DC = ', num2str(I_line_B_DC), ' A', + newline]);
disp('Bus voltages:');
disp(['Voltage at Cook Plant bus:                            Vs_Cook_DC = ', num2str(Vs_Cook_DC), ' V']);
disp(['Voltage at Battle Creek bus:                          V_BC_DC = Vs_BC_DC = Vr_BC_DC = ', num2str(V_BC_DC), ' V']);
disp(['Voltage at Detroit bus:                               Vr_Det_DC = ', num2str(Vr_Det_DC), ' V', + newline]);
disp(['Voltage regulation (VR):                              VR = ', num2str(VR_DC), '%', + newline]);
disp('Line losses:');
disp(['Lines loss b/t Cook Plant and Battle Creek (line A):  P_loss_A_DC = ', num2str(P_loss_A_DC), ' W']);
disp(['Lines loss b/t Battle Creek and Detroit (line B):     P_loss_B_DC = ', num2str(P_loss_B_DC), ' W', + newline]);

%% end functions
% modified open-source functions for converting rectangular to polar
% r2p is for debugging and displaying rectangular quantities in polar
function [] = r2p(z)
% z = a + ib where a is real part & b is imaginary part
% magnitude = sqrt(a^2+b^2)
% angle = inver_tan(b/a)
a = real(z);
b = imag(z);
magnitude = sqrt(a^2 + b^2);
if a > 0 && b > 0
    angle = (atan(abs(b/a)))*180/pi;
elseif a < 0 && b > 0
    angle = 180-((atan(abs(b/a)))*180/pi);
elseif a < 0 && b < 0
    angle = 180 + ((atan(abs(b/a)))*180/pi);
elseif a > 0 && b < 0
    angle =- ((atan(abs(b/a)))*180/pi);
elseif a == 0
    angle = 90;
end
disp([num2str(magnitude), '<', num2str(angle), + newline]);
end

% r2p2 is for returning the angle and magnitude for calculation purposes
% and for formatting the output
function [magnitude, angle] = r2p2(z)
% z = a + ib where a is real part & b is imaginary part
% magnitude = sqrt(a^2+b^2)
% angle = inver_tan(b/a)
a = real(z);
b = imag(z);
magnitude = sqrt(a^2 + b^2);
if a > 0 && b > 0
    angle = (atan(abs(b/a)))*180/pi;
elseif a < 0 && b > 0
    angle = 180-((atan(abs(b/a)))*180/pi);
elseif a < 0 && b < 0
    angle = 180 + ((atan(abs(b/a)))*180/pi);
elseif a > 0 && b < 0
    angle =- ((atan(abs(b/a)))*180/pi);
elseif a == 0
    angle = 90;
end
end





